from abc import ABC, abstractmethod
from PIL import Image
from typing import Generic, TypeVar, Tuple

from render import Renderable

# class View(ABC):
#   # @abstractmethod
#   # def canvas(self) -> Image:
#   #   raise NotImplementedError
#
#   # @abstractmethod
#   # def paint(self, img: Image) -> bool:
#   #   raise NotImplementedError
#
#   @abstractmethod
#   def render(self, renderable: Renderable) -> bool:
#     raise NotImplementedError
#
#   @abstractmethod
#   def width(self) -> int:
#     raise NotImplementedError
#
#   @abstractmethod
#   def height(self) -> int:
#     raise NotImplementedError
#
#   @abstractmethod
#   def xy_offset(self) -> Tuple[int, int]:
#     raise NotImplementedError

# class Viewable(ABC):
#   @abstractmethod
#   def type(self) -> Any:
#     raise NotImplementedError

class View:
  def __init__(self, renderer=dict()):
    self.__view = view
    self.__renderer = renderer

  def register(self, viewable, renderer):
    self.__renderer[viewable] = renderer

  def render(self, viewable):
    renderer = self.__renderer[viewable.type()]
    renderer.render(viewable)
