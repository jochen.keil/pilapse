import time
import RPi.GPIO as GPIO
from enum import Enum, IntEnum, auto

class KeyCode(IntEnum):
  Key_1 = 21
  Key_2 = 20
  Key_3 = 16
  Up    = 6
  Down  = 19
  Left  = 5
  Right = 26
  Press = 13

class KeyEvent(IntEnum):
  Hold    = auto()
  Press   = auto()
  Release = auto()

# class KeyHandler(ABC):
#   @abstractmethod
#   def handle(self, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int):
#     raise NotImplementedError

# class KeyListener:
#   # TODO
#   # https://raspberrypi.stackexchange.com/questions/76667/debouncing-buttons-with-rpi-gpio-too-many-events-detected
#   def __init__(self, key_handler: KeyHandler, bouncetime=100):
#     self.__state = set()
#     self.__key_handler = key_handler
#
#     GPIO.setmode(GPIO.BCM)
#     for key_code in KeyCode:
#       GPIO.setup(key_code, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#       # GPIO.add_event_detect(key_code, GPIO.BOTH, bouncetime=bouncetime)
#       GPIO.add_event_detect(key_code, GPIO.BOTH)
#
#   def __del__(self):
#     GPIO.cleanup()
#
#   def run(self):
#     t = time.time_ns()
#     for key_code in KeyCode:
#       if GPIO.event_detected(key_code):
#         # key press
#         if key_code not in self.__state:
#           self.__state.add(key_code)
#           self.__key_handler.handle(key_code, KeyEvent.Press, t)
#
#         # key release
#         elif key_code in self.__state:
#           self.__state.discard(key_code)
#           self.__key_handler.handle(key_code, KeyEvent.Release, t)

# class KeyBindings:
#   def __init__(self):
#     self.__bindings = dict()
#
#   def bind(self, key_code: KeyCode, key_event: KeyEvent, fn, *args, **kwargs):
#     self.__bindings[(key_code, key_event)] = lambda: fn(*args, **kwargs)
#
#   def unbind(self, key_code: KeyCode, key_event: KeyEvent):
#     try:
#       self.__bindings.pop((key_code, key_event))
#     except KeyError:
#       pass
#
#   def on_hold(self, key_code: KeyCode, fn, *args, **kwargs):
#     self.bind(key_code, KeyEvent.Hold, fn, *args, **kwargs)
#
#   def on_press(self, key_code: KeyCode, fn, *args, **kwargs):
#     self.bind(key_code, KeyEvent.Press, fn, *args, **kwargs)
#
#   def on_release(self, key_code: KeyCode, fn, *args, **kwargs):
#     self.bind(key_code, KeyEvent.Release, fn, *args, **kwargs)
