from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Tuple, TypeVar
from PIL import Image, ImageDraw, ImageFont
from surface import Surface

import math
from components import ListMenu
# from text_view import TextView
from state import ListState

from PIL import Image, ImageDraw
from typing import Optional
from components import StringInput
# from text_view import TextView

T = TypeVar('T')

class View(ABC):
  @abstractmethod
  def render(self, obj: T, surface: Surface) -> None:
    raise NotImplementedError

@dataclass
class TextViewConfig:
  width: int
  height: int
  mode: str
  font: str
  spacing: int
  bg_color: int
  fg_color: int
  xy_offset: Tuple[int, int]

class TextView:
  def __init__(self, config: TextViewConfig):
    self.__config = config
    self.__img = Image.new(size=(config.width, config.height), mode=config.mode)
    self.__draw = ImageDraw.Draw(self.__img)
    self.__font = ImageFont.load(config.font)

  @property
  def config(self) -> TextViewConfig:
    return self.__config

  @property
  def img(self) -> Image:
    return self.__img

  @property
  def draw(self) -> ImageDraw:
    return self.__draw

  @property
  def font(self) -> ImageFont:
    return self.__font

  def clear(self) -> None:
    width = self.img.width
    height = self.img.height
    fill = self.config.bg_color
    self.draw.rectangle([0, 0, width, height], fill=fill)

  def text_size(self, text: str) -> Tuple[int, int]:
    spacing = self.config.spacing
    return self.draw.multiline_textsize(text=text, font=self.font, spacing=spacing)

  def draw_text(self, pos: Tuple[int, int], text: str) -> None:
    fill = self.config.fg_color
    spacing = self.config.spacing
    self.draw.multiline_text(pos, text, font=self.font, fill=fill, spacing=spacing)

class ListMenuView(TextView, View):
  def __init__(self, config: TextViewConfig) -> None:
    super().__init__(config)

    self.__state = None

    self.__menu_offset = 0
    self.__last_active_item = 0
    self.__line_height = self.text_size(18*'\n')[1] / 19

  def __menu_items(self, list_state: ListState) -> str:
    idx = list_state.current_index()
    names = list_state.names()
    space = ' '
    marked = '>' + space
    nomark = 2 * space
    return '\n'.join([ marked + name if n == idx else nomark + name
                       for n, name in enumerate(names) ])

  def __overflow_offset(self, list_state: ListState) -> int:
    menu_lines = len(list_state.names())
    cursor_line = list_state.current_index() + 1
    menu_offset = self.__menu_offset

    line_height = self.__line_height
    display_lines = math.floor(self.img.height / line_height)

    menu_top = menu_offset == 0
    menu_bottom = menu_offset == menu_lines - display_lines

    cursor_top = cursor_line == menu_offset + 1
    cursor_bottom = cursor_line == menu_offset + display_lines

    move_next = self.__last_active_item <= list_state.current_index()
    move_prev = self.__last_active_item > list_state.current_index()

    shift_menu_up = cursor_bottom and move_next and not menu_bottom
    shift_menu_down = cursor_top and move_prev and not menu_top

    if shift_menu_up:
      menu_offset = menu_offset + 1

    if shift_menu_down:
      menu_offset = menu_offset - 1

    self.__menu_offset = menu_offset
    self.__last_active_item = list_state.current_index()

    return menu_offset * line_height

  def render(self, list_menu: ListMenu, surface: Surface) -> None:
    # if self.__state == list_menu.state():
    #   return
    # else:
    #   self.__state = list_menu.state()

    self.clear()

    height = self.img.height
    items = self.__menu_items(list_menu.state())
    items_height = self.text_size(items)[1]

    if items_height < height:
      y_offset = (height - items_height) / 2
    else:
      y_offset = self.config.xy_offset[1] - self.__overflow_offset(list_menu.state())

    xy = (self.config.xy_offset[0], math.floor(y_offset))

    self.draw_text(xy, items)

    surface.paste(self.img)

class StringInputView(TextView, View):
  def __init__(self, config: TextViewConfig, prompt: Optional[str] = None):
    super().__init__(config)
    self.__state = None
    self.__prompt = prompt

  @property
  def prompt(self) -> Optional[str]:
    return self.__prompt

  def render(self, string_input: StringInput, surface: Surface) -> None:
    # if self.__state == string_input.state():
    #   return
    # else:
    #   self.__state = string_input.state()

    self.clear()

    current_idx = string_input.state().current_index()
    cursor = current_idx * ' ' + '^'

    prompt = self.prompt + '\n' if self.prompt is not None else ''
    selection = string_input.current_input()

    text = prompt + selection + '\n' + cursor
    _, text_height = self.text_size(text)

    x = self.config.xy_offset[0]
    y = (self.img.height - text_height) / 2

    self.draw_text((x, y), text)

    surface.paste(self.img)

# import math
# from abc import ABC, abstractmethod
# from PIL import Image, ImageDraw, ImageFont
#
# from state import StateType, ListState, StringState
# from typing import Generic, TypeVar, Tuple
#
# from view import View
#
# R = TypeVar('R')
#
# class TextView:
#   def __init__(self, config, canvas, xy_offset=None, rotate=None, convert=None):
#     self.__canvas = canvas
#     self.__config = config
#     self.__xy_offset = xy_offset if xy_offset is not None else (0, 0)
#     self.__rotate = rotate
#     self.__convert = convert
#     self.__font = ImageFont.load(config.font)
#
#     self.__img = Image.new(size=(config.width, config.height), mode='RGB')
#     self.__draw = ImageDraw.Draw(self.__img)
#
#   def register(self, viewable_type: ViewableType, renderer: Renderer):
#     pass
#
#   def width(self) -> int:
#     return self.__config.width
#
#   def height(self) -> int:
#     return self.__config.height
#
#   def xy_offset(self) -> Tuple[int, int]:
#     return self.__xy_offset
#
#   def clear(self):
#     width = self.__config.width
#     height = self.__config.height
#     fill = self.__config.bg_color
#     outline = self.__config.fg_color
#     self.__draw.rectangle([0, 0, width, height], fill=fill)
#
#   def text_size(self, text: str) -> None:
#     font = self.__font
#     spacing = self.__config.spacing
#     return self.__draw.multiline_textsize(text=text, font=font, spacing=spacing)
#
#   def draw_text(self, xy_pos: Tuple[int, int], text: str) -> None:
#     font = self.__font
#     fill = self.__config.fg_color
#     spacing = self.__config.spacing
#     self.__draw.multiline_text(xy, text, font=font, fill=fill, spacing=spacing)
#
#   def paint(self, img: Image) -> bool:
#   # def paste(self):
#     tmp = self.__img
#     if self.__convert is not None:
#       tmp = tmp.convert(self.__convert)
#     if self.__rotate is not None:
#       tmp = tmp.rotate(self.__rotate)
#     self.__canvas.paste(tmp)
#
# # class MenuView:
# #   def __init__(self, config, canvas, xy_offset=None, rotate=None, convert=None):
# #     self.__canvas = canvas
# #     self.__config = config
# #     self.__xy_offset = xy_offset if xy_offset is not None else (0, 0)
# #     self.__rotate = rotate
# #     self.__convert = convert
# #     self.__font = ImageFont.load(config.font)
# #
# #     self.__img = Image.new(size=(config.width, config.height), mode='RGB')
# #     self.__draw = ImageDraw.Draw(self.__img)
# #
# #     self.__menu_offset = 0
# #     self.__last_active_item = 0
# #     self.__line_height = self.__text_size(18*'\n')[1] / 19
# #
# #     self.__render_function = { StateType.List: self.__render_list_selection
# #                              , StateType.String: self.__render_string_selection
# #                              }
# #
# #   def load_font(self, fontpath):
# #     self.__font = ImageFont.load(fontpath)
# #
# #   def clear(self):
# #     width = self.__config.width
# #     height = self.__config.height
# #     fill = self.__config.bg_color
# #     outline = self.__config.fg_color
# #     self.__draw.rectangle([0, 0, width, height], fill=fill)
# #
# #   def outline(self):
# #     width = self.__config.width
# #     height = self.__config.height
# #     fill = self.__config.bg_color
# #     outline = self.__config.fg_color
# #     self.__draw.rectangle([0, 0, width-1, height-1], fill=fill, outline=outline, width=1)
# #
# #   def render(self, state):
# #     if state.state_type in self.__render_function:
# #       self.__render_function[state.state_type](state)
# #     else:
# #       raise RuntimeError(f"No renderer available for {state.state_type.name}")
# #
# #   def __render_string_selection(self, string_selection: StringState):
# #     selection = string_selection.get_selection()
# #
# #     current_idx = string_selection.current_char_index()
# #     cursor = current_idx * ' ' + '^'
# #
# #     prompt = string_selection.prompt
# #     prompt = prompt + '\n' if prompt is not None else ''
# #
# #     text = prompt + selection + '\n' + cursor
# #     _, height = self.__text_size(text)
# #
# #     x = self.__xy_offset[0]
# #     y = (self.__config.height - height) / 2
# #
# #     self.__draw_text((x, y), text)
#
