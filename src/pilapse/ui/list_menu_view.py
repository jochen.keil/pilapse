import math
from components import ListMenu
from text_view import TextView
from states import ListState

class ListMenuView(TextView):
  def __init__(self, surface, config) -> None:
    super().__init__(surface, config)

    self.__menu_offset = 0
    self.__last_active_item = 0
    self.__line_height = self.text_size(18*'\n')[1] / 19

  def __menu_items(self, list_state: ListState) -> str:
    idx = list_state.current_index()
    names = list_state.names()
    space = ' '
    marked = '>' + space
    nomark = 2 * space
    return '\n'.join([ marked + name if n == idx else nomark + name
                       for n, name in enumerate(names) ])

  def __overflow_offset(self, list_state: ListState) -> int:
    menu_lines = len(list_state.names())
    cursor_line = list_state.current_index() + 1
    menu_offset = self.__menu_offset

    line_height = self.__line_height
    display_lines = math.floor(self.__config.height / line_height)

    menu_top = menu_offset == 0
    menu_bottom = menu_offset == menu_lines - display_lines

    cursor_top = cursor_line == menu_offset + 1
    cursor_bottom = cursor_line == menu_offset + display_lines

    move_next = self.__last_active_item <= list_state.current_index()
    move_prev = self.__last_active_item > list_state.current_index()

    shift_menu_up = cursor_bottom and move_next and not menu_bottom
    shift_menu_down = cursor_top and move_prev and not menu_top

    if shift_menu_up:
      menu_offset = menu_offset + 1

    if shift_menu_down:
      menu_offset = menu_offset - 1

    self.__menu_offset = menu_offset
    self.__last_active_item = list_state.current_index()

    return menu_offset * line_height

  def render(self, list_menu: ListMenu) -> None:
    height = self.surface.height
    items = self.__menu_items(list_menu.state())
    items_height = self.text_size(items)[1]

    if items_height < height:
      y_offset = (height - items_height) / 2
    else:
      y_offset = self.config.xy_offset[1] - self.__overflow_offset(list_menu.state())

    xy = (self.config.xy_offset[0], math.floor(y_offset))

    self.draw_text(xy, items)
