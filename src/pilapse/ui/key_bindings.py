import time
# import RPi.GPIO as GPIO
from typing import Callable, Generic, TypeVar
from enum import Enum, IntEnum, auto
from key_handler import KeyHandler
from keys import KeyCode, KeyEvent
from components import ListMenu, StringInput

R = TypeVar('R')
T = TypeVar('T')

# class KeyHandler(ABC):
#   @abstractmethod
#   def handle(self, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int) -> R:
#     raise NotImplementedError

class KeyBindings(Generic[T]):
  def __init__(self):
    self.__key_bindings = dict()

  # def bind(self, key_code: KeyCode, key_event: KeyEvent, fn, *args, **kwargs) -> None:
  def bind(self, key_code: KeyCode, key_event: KeyEvent, fn) -> None:
    self.__key_bindings[(key_code, key_event)] = fn

  def unbind(self, key_code: KeyCode, key_event: KeyEvent) -> None:
    try:
      self.__key_bindings.pop((key_code, key_event))
    except KeyError:
      pass

  # def on_hold(self, key_code: KeyCode, fn, *args, **kwargs) -> None:
  #   self.bind(key_code, KeyEvent.Hold, fn, *args, **kwargs)

  def on_hold(self, key_code: KeyCode, fn) -> None:
    self.bind(key_code, KeyEvent.Hold, fn)

  # def on_press(self, key_code: KeyCode, fn, *args, **kwargs) -> None:
  #   self.bind(key_code, KeyEvent.Press, fn, *args, **kwargs)

  def on_press(self, key_code: KeyCode, fn) -> None:
    self.bind(key_code, KeyEvent.Press, fn)

  # def on_release(self, key_code: KeyCode, fn, *args, **kwargs) -> None:
  #   self.bind(key_code, KeyEvent.Release, fn, *args, **kwargs)

  def on_release(self, key_code: KeyCode, fn) -> None:
    self.bind(key_code, KeyEvent.Release, fn)

  def handler(self, key_code: KeyCode, key_event: KeyEvent) -> Callable[[T], R]:
    return self.__key_bindings[(key_code, key_event)]

class ListMenuKeyBindings(KeyBindings[ListMenu], KeyHandler):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    for key_event in [KeyEvent.Hold, KeyEvent.Release]:
      self.bind(KeyCode.Up, key_event, lambda o: o.prev())
      self.bind(KeyCode.Down, key_event, lambda o: o.next())
    self.on_release(KeyCode.Press, lambda o: o.current_id())

  def handle(self, obj: ListMenu, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int) -> R:
    try:
      handler = self.handler(key_code, key_event)
      return handler(obj)
    except KeyError:
      return None

class StringInputKeyBindings(KeyBindings[ListMenu], KeyHandler):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    for key_event in [KeyEvent.Hold, KeyEvent.Release]:
      self.bind(KeyCode.Right, key_event, lambda o: o.next())
      self.bind(KeyCode.Left, key_event, lambda o: o.prev())
      self.bind(KeyCode.Up, key_event, lambda o: o.prev_char())
      self.bind(KeyCode.Down, key_event, lambda o: o.next_char())
      self.bind(KeyCode.Key_1, key_event, lambda o: o.state().delete())
      self.bind(KeyCode.Key_2, key_event, lambda o: o.state().backspace())
    self.on_release(KeyCode.Press, lambda o: o.current_input())

  def handle(self, obj: ListMenu, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int) -> R:
    try:
      handler = self.handler(key_code, key_event)
      return handler(obj)
    except KeyError:
      return None
