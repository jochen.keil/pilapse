import math
from abc import ABC, abstractmethod
from PIL import Image, ImageDraw, ImageFont

from state import StateType, ListState, StringState
from typing import Generic, TypeVar, Tuple

from view import View

R = TypeVar('R')

class TextView:
  def __init__(self, config, xy_offset=None, rotate=None, convert=None):
    self.__config = config
    self.__xy_offset = xy_offset if xy_offset is not None else (0, 0)
    self.__rotate = rotate
    self.__convert = convert
    self.__font = ImageFont.load(config.font)

  def configure(self, img: Image) -> None:
    self.__img = img
    self.__mode = img.mode
    self.__width = img.width
    self.__height = img.height
    self.__draw = ImageDraw.Draw(img)

  @property
  def config(self) -> int:
    return self.__config

  @property
  def width(self) -> int:
    return self.__width

  @property
  def height(self) -> int:
    return self.__height

  @property
  def xy_offset(self) -> Tuple[int, int]:
    return self.__xy_offset

  @property
  def draw(self) -> ImageDraw:
    return self.__draw

  @property
  def font(self) -> ImageFont:
    return self.__font

  def clear(self):
    fill = self.config.bg_color
    self.draw.rectangle([0, 0, self.width, self.height], fill=fill)

  def text_size(self, text: str) -> None:
    spacing = self.config.spacing
    return self.draw.multiline_textsize(text=text, font=self.font, spacing=spacing)

  def draw_text(self, xy_pos: Tuple[int, int], text: str) -> None:
    fill = self.config.fg_color
    spacing = self.config.spacing
    self.__draw.multiline_text(xy, text, font=self.font, fill=fill, spacing=spacing)
