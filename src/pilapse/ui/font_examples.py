import os
from PIL import Image, ImageDraw, ImageFont

def font_examples(prefix):
  fonts = [ "fonts/Gomme10x20n.pil", "fonts/gohufont-11.pil"
          , "fonts/gohufont-14.pil", "fonts/knxt.pil"
          ]

  img = Image.new(mode='RGB', size=(300, 300), color=0x000000)
  draw = ImageDraw.Draw(img)

  height = 0
  for n, font in enumerate(fonts):
    fontname = os.path.splitext(os.path.split(font)[-1])[0]
    fnt = ImageFont.load(font)
    w, h = fnt.getsize(fontname)
    height = 10 + height + h
    draw.text((10, height), f'{fontname} ({h}px)', font=fnt, fill=0xffffff)

  img.save(prefix + '.png')

if __name__ == "__main__":
  font_examples('font_examples')
