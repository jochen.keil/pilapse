import os, os.path as path
from string import ascii_letters, digits, punctuation
from argparse import ArgumentParser
from Framebuffer import Image

from ui import MenuView
from state import State, CharInput, ListSelection
from config import Config
from keys import KeyLoop, KeyEvent, KeyCode, KeyHold

FB_DEV = "/dev/fb1"
REPEAT_TIME = 200

class UI:
  def __init__(self, fontdir: str, control: KeyLoop, view: MenuView):
    self.__hold = False
    self.__view = view
    self.__control = control
    self.__control.register(self.key_handler)

    # self.__control.register(KeyHold(KeyCode.KEY3, self.key_handler).handler)
    for kc in [KeyCode.KEY_UP, KeyCode.KEY_DOWN, KeyCode.KEY_LEFT, KeyCode.KEY_RIGHT]:
      self.__control.register(KeyHold(kc, self.key_handler, timeout=REPEAT_TIME, repeat=0).handler)

    menu_items = \
        [ "Fonts", "Input", "Quit"
        , "Mode", "ISO", "Aperture", "Shutterspeed"
        , "Menu Item 1", "Menu Item 2", "Menu Item 3"
        , "Menu Item 4", "Menu Item 5", "Menu Item 6"
        , "Menu Item 7", "Menu Item 8", "Menu Item 9"
        , "Menu Item 10", "Menu Item 11", "Menu Item 12"
        , "Menu Item 13", "Menu Item 14", "Menu Item 15"
        ]

    fonts = list(filter(lambda f: path.splitext(f)[1] == '.pil', os.listdir(fontdir)))
    fontnames = list(map(lambda f: path.splitext(path.basename(f))[0], fonts))

    self.__fonts = dict(zip(fontnames, map(lambda f: path.join(fontdir, f), fonts)))

    self.__states = { 'Menu': ListSelection('Menu', menu_items)
                    , 'Fonts': ListSelection('Fonts', list(fontnames))
                    , 'Input': CharInput('Input', prompt='Enter some text:', chars=ascii_letters + digits + punctuation)
                    }

    self.menu_state = self.states['Menu']

  def run(self):
    self.__control.run()

  @property
  def states(self):
    return self.__states

  @property
  def menu_state(self):
    return self.__menu_state

  @menu_state.setter
  def menu_state(self, state):
    self.__menu_state = state

  def key_handler(self, key_code, key_event, ev_time_ns):
    if self.__menu_state.type() == State.ListSelection:
      self.list_selection_handler(key_code, key_event, ev_time_ns)
    elif self.__menu_state.type() == State.CharInput:
      self.char_input_handler(key_code, key_event, ev_time_ns)

  def __key_hold_handler(self, key_event, handler):
    if key_event == KeyEvent.Release:
      if self.__hold:
        self.__hold = False
      else:
        handler()

    elif key_event == KeyEvent.Hold:
      self.__hold = True
      handler()

  def list_selection_handler(self, key_code, key_event, ev_time_ns):
    def handler():
      if key_code == KeyCode.KEY_UP:
        self.menu_state.prev()
      elif key_code == KeyCode.KEY_DOWN:
        self.menu_state.next()
      elif key_code == KeyCode.KEY_PRESS:
        if self.menu_state.current() == 'Quit':
          self.__control.stop()
        elif self.menu_state.current() == 'Input':
          self.menu_state = self.states['Input']
        elif self.menu_state.current() == 'Fonts':
          self.menu_state = self.states['Fonts']
        elif self.menu_state.tag == 'Fonts':
          font = self.__fonts[self.menu_state.current()]
          self.__view.load_font(font)
          self.menu_state = self.states['Menu']

    self.__key_hold_handler(key_event, handler)
    self.render()

  def char_input_handler(self, key_code, key_event, ev_time_ns):
    def handler():
      if key_code == KeyCode.KEY_UP:
        self.menu_state.current().next()
      elif key_code == KeyCode.KEY_DOWN:
        self.menu_state.current().prev()
      elif key_code == KeyCode.KEY_LEFT:
        self.menu_state.prev()
      elif key_code == KeyCode.KEY_RIGHT:
        self.menu_state.next()
      elif key_code == KeyCode.KEY1:
        self.menu_state.delete()
      elif key_code == KeyCode.KEY2:
        self.menu_state.backspace()
      elif key_code == KeyCode.KEY_PRESS:
        print(self.menu_state.text())
        self.menu_state = self.states['Menu']

    self.__key_hold_handler(key_event, handler)
    self.render()

  def render(self):
    self.__view.clear()
    self.__view.outline()
    self.__view.render(self.menu_state)
    self.__view.paste()

def main():
  parser = ArgumentParser()
  parser.add_argument('--font', type=str, default='fonts/gohufont-11.pil')
  parser.add_argument('--font-dir', type=str, default='fonts')
  parser.add_argument('--spacing', type=int, default=1)
  args = parser.parse_args()

  with Image(FB_DEV) as fbi:
    xres = fbi.get_vscreeninfo().xres
    yres = fbi.get_vscreeninfo().yres

    config = Config(width=xres, height=yres, spacing=args.spacing, font=args.font)
    view = MenuView(config, fbi.get_image(), xy_offset=(5, 2), convert='1', rotate=180)
    control = KeyLoop()

    if path.isabs(args.font_dir):
      fontdir = args.font_dir
    else:
      fontdir = path.join(path.dirname(path.normpath(__file__)), args.font_dir)

    ui = UI(fontdir, control, view)
    ui.render()
    ui.run()

    view.clear()
    view.paste()

if __name__ == "__main__":
  main()
