from abc import ABC, abstractmethod
from keys import KeyCode, KeyEvent
from typing import Any, Callable, Generic, TypeVar

S = TypeVar('S')

class State(ABC, Generic[S]):
  @abstractmethod
  def state(self) -> S:
    raise NotImplementedError

# class KeyHandler(Generic[S]):
#   def __init__(self, callback: Callable[[State[S]], bool] = None):
#     self.__callback = callback
#     self.__key_bindings = dict()
#
#   @property
#   def callback(self):
#     return self.__callback
#
#   @callback.setter
#   def callback(self, callback):
#     self.__callback = callback
#
#   def bind(self, key_code: KeyCode, key_event: KeyEvent, fn: Callable, *args, **kwargs) -> None:
#     self.__key_bindings[(key_code, key_event)] = lambda: fn(*args, **kwargs)
#
#   def on_hold(self, key_code: KeyCode, fn: Callable, *args, **kwargs) -> None:
#     self.bind(key_code, KeyEvent.Hold, fn, *args, **kwargs)
#
#   def on_press(self, key_code: KeyCode, fn: Callable, *args, **kwargs) -> None:
#     self.bind(key_code, KeyEvent.Press, fn, *args, **kwargs)
#
#   def on_release(self, key_code: KeyCode, fn: Callable, *args, **kwargs) -> None:
#     self.bind(key_code, KeyEvent.Release, fn, *args, **kwargs)
#
#   def handle(self, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int) -> bool:
#     try:
#       return self.__key_bindings[(key_code, key_event)]()
#     except KeyError:
#       return False

# class Viewable(ABC, Generic[R]):
#   @abstractmethod
#   def view(self) -> R:
#     raise NotImplementedError

class UIComponent(KeyHandler[S], State[S]):
  pass
