from control import AbstractControl
from state import ListSelection, CharInput

root_menu = ListSelection('root',
    [ "Fonts", "Input", "Quit"
    , "Mode", "ISO", "Aperture", "Shutterspeed"
    , "Menu Item 1", "Menu Item 2", "Menu Item 3"
    , "Menu Item 4", "Menu Item 5", "Menu Item 6"
    , "Menu Item 7", "Menu Item 8", "Menu Item 9"
    , "Menu Item 10", "Menu Item 11", "Menu Item 12"
    , "Menu Item 13", "Menu Item 14", "Menu Item 15"
    ])

input_menu = CharInput('Input', chars=ascii_letters + digits + punctuation)

def ListSelectionController(AbstractControl):
  def run(self, state: ListSelection):
    if key_code == KeyCode.KEY_UP:
      state.prev()
    elif key_code == KeyCode.KEY_DOWN:
      state.next()
    elif key_code == KeyCode.KEY_PRESS:
      return state.current()

def CharInputController(AbstractControl):
  def handle(self, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int):
    if key_code == KeyCode.KEY_UP:
      self.state.next_char()
    elif key_code == KeyCode.KEY_DOWN:
      self.state.prev_char()
    if key_code == KeyCode.KEY_LEFT:
      self.state.prev()
    elif key_code == KeyCode.KEY_RIGHT:
      self.state.next()
    elif key_code == KeyCode.KEY1:
      self.state.delete()
    elif key_code == KeyCode.KEY2:
      self.state.backspace()
    elif key_code == KeyCode.KEY_PRESS:
      return True
    return False
    #   return state.current()

def CharSelectionController(AbstractControl):
  def dispatch(self, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int):
    if key_code == KeyCode.KEY_UP:
      self.state.next()
    elif key_code == KeyCode.KEY_DOWN:
      self.state.prev()
    return True

class StateController:
  def register(self, item, action):
    self.__actions[item] = action

class MenuManager:
  def __init__(self, state, view, control):
    self.__view = view
    self.__state = state
    self.__control = control

  @property
  def view(self):
    return self.__view

  @property
  def state(self):
    return self.__state

  @property
  def control(self):
    return self.__control

  def run(self):
    self.control(state)

def foo(state, dispatcher):
  handled = dispatch(..)
  if not handled and state.has_next_state():
    dispatch(state.next_state())

class ListSelectionWidget:
  def __init__(self, list_selection: ListSelection):
    self.__list_selection = list_selection

  def register(self, item, callback):

##############

from core import Core

def main():

  list_selection_controller = ListSelectionController()

  core = Core()
  view = View()
  controller = Controller()

  core.register(view.render)
  core.register(controller.read_keys)

  core.run()
