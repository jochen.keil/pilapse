from abc import ABC, abstractmethod
from typing import Any, Dict, Generic, TypeVar
# from renderable import Renderable
from components import ListMenu, StringInput
# from views import TextView

from state import ListState

TextView = str
C = TypeVar('C')

class Renderer(ABC, Generic[C]):
  @abstractmethod
  def render(self, c: C) -> None:
    raise NotImplementedError

# class View(ABC, Generic[T]):
#   @abstractmethod
#   def render(self, T) -> None:
#     raise NotImplementedError

  # @abstractmethod
  # def register(self, Renderable) -> None:
  #   raise NotImplementedError

# StringInput(StringState):
#   ..

class StringInputRenderer(Renderer[StringInput]):
  def render(self, string_input: StringInput) -> None:
    pass

T = TypeVar('T')

class View:
  def __init__(self) -> None:
    self.__render_registry: Dict[str, Any] = dict()

  def register(self, cls: T, renderer: Renderer[T]) -> None:
    self.__render_registry[cls.__class__.__name__] = renderer

  def render(self, cls: T) -> None:
    renderer = self.__render_registry[cls.__class__.__name__]
    renderer(cls)

# class TextView(View[StringInput]:
#   register(cls: StringInput, renderer: Renderer[StringInput])
#   register(StringInput.__name__, StringInputRenderer())
#
#   string_input = StringInput
#   render(cls: UIComponent[TextView]) -> None:
#     if cls.__class__.__name__ in self.__renderer:
#       renderer = self.__renderer[cls.__name__]
#       renderer.render(string_input)

# class StringInputRenderer(Renderable[TextView, StringInput]):
# class StringInputRenderer(Renderer):
class StringInputRenderer2:
  def __init__(self, view: TextView, string_input: StringInput):
    self.__view = view
    self.__string_input = string_input

  @property
  def view(self) -> TextView:
    return self.__view

  @property
  def string_input(self) -> StringInput:
    return self.__string_input

  def render(self) -> None:
    selection = self.string_input.get_selection()

    current_idx = self.string_input.current_char_index()
    cursor = current_idx * ' ' + '^'

    prompt = self.string_input.prompt
    prompt = prompt + '\n' if prompt is not None else ''

    text = prompt + selection + '\n' + cursor
    _, height = self.view.text_size(text)

    x = self.view.xy_offset[0]
    y = (self.view.height - height) / 2

    self.view.draw_text((x, y), text)

# # class ListMenuRenderable(Renderable[TextView, ListMenu]):
# class ListMenuRenderable:
#   def __init__(self, view: TextView, list_menu: ListMenu):
#     self.__view = view
#
#     self.__menu_offset = 0
#     self.__last_active_item = 0
#     self.__line_height = self.__text_size(18*'\n')[1] / 19
#
#   def __menu_items(self, list_selection):
#     idx = list_selection.get_selection_index()
#     names = list_selection.get_names()
#     marked = '>' + space
#     nomark = 2 * space
#     return '\n'.join([ marked + name if n == idx else nomark + name
#                        for n, name in enumerate(names) ])
#
#   def __overflow_offset(self, view: TextView, selection: ListState):
#     menu_lines = len(selection.items())
#     cursor_line = selection.active() + 1
#     menu_offset = self.__menu_offset
#
#     line_height = self.__line_height
#     display_lines = math.floor(self.__config.height / line_height)
#
#     menu_top = menu_offset == 0
#     menu_bottom = menu_offset == menu_lines - display_lines
#
#     cursor_top = cursor_line == menu_offset + 1
#     cursor_bottom = cursor_line == menu_offset + display_lines
#
#     move_next = self.__last_active_item <= selection.active()
#     move_prev = self.__last_active_item > selection.active()
#
#     shift_menu_up = cursor_bottom and move_next and not menu_bottom
#     shift_menu_down = cursor_top and move_prev and not menu_top
#
#     if shift_menu_up:
#       menu_offset = menu_offset + 1
#
#     if shift_menu_down:
#       menu_offset = menu_offset - 1
#
#     self.__menu_offset = menu_offset
#     self.__last_active_item = selection.active()
#
#     return menu_offset * line_height
#
#   def render(self):
#   # def __render_list_selection(self, selection: ListState):
#     view = self.__view
#     string_input_state = string_input
#
#     height = view.height
#     items = self.__menu_items(selection)
#     items_height = self.__text_size(items)[1]
#
#     if items_height < height:
#       y_offset = (height - items_height) / 2
#     else:
#       y_offset = view.xy_offset[1] - self.__overflow_offset(selection)
#
#     xy = (view.xy_offset[0], math.floor(y_offset))
#
#     view.draw_text(xy, items)
