import yaml

class Menu:
  def __init__(self):
    self.state = None

  def select_next(self):
    pass

  def select_prev(self):
    pass

  def get_active_selection(self) -> str:
    pass

  def get_selection_items(self) -> [str]:
    pass
