from typing import Generic, TypeVar, Sequence, Optional

T = TypeVar('T')

class Stack(Generic[T]):
  def __init__(self, items: Sequence[T] = []):
    self.__stack = items

  def push(self, v: T):
    self.__stack.append(v)

  def pop(self) -> Optional[T]:
    if self.is_empty():
      return None
    else:
      return self.__stack.pop()

  def top(self) -> Optional[T]:
    if self.is_empty():
      return None
    else:
      return self.__stack[-1]

  def is_empty(self) -> bool:
    return len(self.__stack) == 0
