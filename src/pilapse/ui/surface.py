from PIL import Image, ImageDraw
from framebuffer import Framebuffer

class Surface:
  @staticmethod
  def open(fp: str, rotate=0, convert=False):
    surface = Surface()
    surface.__fb = Framebuffer.open(fp, rotate, convert)
    # if canvas is not None:
    #   surface.__canvas = canvas # Image.new(size=(surface.width, surface.height), mode='1')
    #   surface.__draw = ImageDraw.Draw(surface.__canvas)
    return surface

  def close(self):
    self.__fb.close()

  def __enter__(self):
    return self

  def __exit__(self, exc_type, exc_val, exc_tb):
    self.close()

  # @property
  # def canvas(self):
  #   return self.__canvas

  # @property
  # def draw(self):
  #   return self.__draw

  @property
  def mode(self):
    return self.__fb.img.mode

  @property
  def width(self):
    return self.__fb.img.width

  @property
  def height(self):
    return self.__fb.img.height

  def paste(self, img: Image) -> None:
    self.__fb.paste(img)

  # def paste(self, img: Image = None) -> None:
  #   if img is not None:
  #     self.__fb.paste(img)
  #   elif self.__canvas is not None:
  #     self.__fb.paste(self.__canvas)
