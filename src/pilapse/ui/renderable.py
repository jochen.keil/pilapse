from abc import ABC, abstractmethod
from typing import Generic, TypeVar

V = TypeVar('V')
R = TypeVar('R')

class Renderable(ABC, Generic[V, R]):
  @abstractmethod
  def __init__(self, view: V, renderable: R) -> None:
    raise NotImplementedError

  @abstractmethod
  def render(self) -> None:
    raise NotImplementedError
