from __future__ import annotations
from string import printable
from enum import Enum, auto
from abc import ABC, abstractmethod
from typing import Any, Sequence, Tuple

# class StateType(Enum):
#   List = auto()
#   Char = auto()
#   String = auto()
#
# class GenericState(ABC):
#   def __init__(self, state_type: StateType, tag: Any = None):
#     self.__tag = tag
#     self.__state_type = state_type
#
#   @property
#   def tag(self):
#     return self.__tag
#
#   @property
#   def state_type(self):
#     return self.__state_type
#
#   @abstractmethod
#   def next(self) -> None:
#     raise NotImplementedError
#
#   @abstractmethod
#   def prev(self) -> None:
#     raise NotImplementedError
#
#   @abstractmethod
#   def current(self) -> Any:
#     raise NotImplementedError
#
#   @abstractmethod
#   def current_index(self) -> int:
#     raise NotImplementedError

class ListState:
  # def __init__(self, items: Sequence[Tuple[str, str]], tag: Any = None):
  def __init__(self, items: Sequence[Tuple[str, str]]):
    assert(len(items) > 0)
    self.__items = items
    self.__current_idx = 0
    # super().__init__(StateType.List, tag)

  # def __eq__(self, other: ListState) -> bool:
  #   print(f'{self}.__eq__({other}): {other is not None and self.__current_idx == other.__current_idx}')
  #   return other is not None and self.__current_idx == other.__current_idx

  def next(self) -> None:
    if self.__current_idx < len(self.__items) - 1:
      self.__current_idx = self.__current_idx + 1

  def prev(self) -> None:
    if self.__current_idx > 0:
      self.__current_idx = self.__current_idx - 1

  def ids(self) -> Sequence[str]:
    return list(map(lambda t: t[0], self.__items))

  def names(self) -> Sequence[str]:
    return list(map(lambda t: t[1], self.__items))

  def current_id(self) -> Tuple[str, str]:
    return self.__items[self.__current_idx][0]

  def current_name(self) -> Tuple[str, str]:
    return self.__items[self.__current_idx][1]

  def current(self) -> Tuple[str, str]:
    return self.__items[self.__current_idx]

  def current_index(self) -> int:
    return self.__current_idx

class CharState:
  # def __init__(self, tag: Any = None, chars: str = printable):
  def __init__(self, chars: str = printable):
    self.__chars = chars
    self.__current_idx = 0
    # super().__init__(StateType.Char, tag)

  # def __eq__(self, other: CharState) -> bool:
  #   return other is not None and self.__current_idx == other.__current_idx

  def next(self) -> None:
    if self.__current_idx < len(self.__chars) - 1:
      self.__current_idx = self.__current_idx + 1
    else:
      self.__current_idx = 0

  def prev(self) -> None:
    if self.__current_idx > 0:
      self.__current_idx = self.__current_idx - 1
    else:
      self.__current_idx = len(self.__chars) - 1

  def current(self) -> str:
    return self.__chars[self.__current_idx]

  def current_index(self) -> int:
    return self.__current_idx

class StringState:
  # def __init__(self, tag: Any = None, chars: str = printable):
  def __init__(self, chars: str = printable):
    self.__chars = chars
    self.__current_idx = 0
    self.__char_selections = []
    self.reset()
    # super().__init__(StateType.String, tag)

  # def __eq__(self, other: StringState) -> bool:
  #   if other is not None and self.__char_selections == other.__char_selections:
  #     return self.__current_idx == other.__current_idx
  #   return False

  def __new_char_selection(self):
    char_selection = CharState(chars=self.__chars)
    return char_selection

  def __current_char_selection(self):
    return self.__char_selections[self.__current_idx]

  def current_char_state(self) -> CharState:
    return self.__char_selections[self.__current_idx]

  def reset(self):
    self.__char_selections = [self.__new_char_selection()]

  def delete(self):
    len_char_selection = len(self.__char_selections)
    if len(self.__char_selections) > 1:
      self.__char_selections.pop(self.__current_idx)
      if self.__current_idx == len(self.__char_selections):
        self.__current_idx = self.__current_idx - 1

  def backspace(self):
    self.prev()
    self.delete()

  def next(self) -> None:
    if self.__current_idx == len(self.__char_selections) - 1:
      self.__char_selections.append(self.__new_char_selection())
    self.__current_idx = self.__current_idx + 1

  def prev(self) -> None:
    if self.__current_idx > 0:
      self.__current_idx = self.__current_idx - 1

  def current(self) -> str:
    return ''.join(map(lambda cs: cs.current(), self.__char_selections))

  def current_index(self) -> int:
    return self.__current_idx
