from time import sleep
from threading import Event, Thread

class Core(Thread):
  def __init__(self, fps=60, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.__fps = fps
    self.__stop = Event()
    self.__callbacks = []

  def stop(self):
    self.__stop.set()

  def register(self, callback):
    self.__callbacks.append(callback)

  def run(self):
    while not self.__stop.is_set():
      for callback in self.__callbacks:
        callback()
      sleep(1.0 / self.__fps)
