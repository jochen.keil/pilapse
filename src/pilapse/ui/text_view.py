from dataclasses import dataclass
from typing import Tuple
from PIL import ImageDraw, ImageFont
from surface import Surface

@dataclass
class Config:
  font: str
  spacing: int
  bg_color: int
  fg_color: int
  xy_offset: Tuple[int, int]

class TextView:
  def __init__(self, surface: Surface, config: Config):
    self.__config = config
    self.__surface = surface
    self.__font = ImageFont.load(config.font)

  @property
  def config(self) -> Config:
    return self.__config

  @property
  def surface(self) -> Surface:
    return self.__surface

  @property
  def font(self) -> ImageFont:
    return self.__font

  def clear(self) -> None:
    fill = self.config.bg_color
    draw = self.surface.draw
    width = self.surface.width
    height = self.surface.height
    draw.rectangle([0, 0, width, height], fill=fill)

  def text_size(self, text: str) -> Tuple[int, int]:
    draw = self.surface.draw
    spacing = self.config.spacing
    return draw.multiline_textsize(text=text, font=self.font, spacing=spacing)

  def draw_text(self, pos: Tuple[int, int], text: str) -> None:
    draw = self.surface.draw
    fill = self.config.fg_color
    spacing = self.config.spacing
    draw.multiline_text(pos, text, font=self.font, fill=fill, spacing=spacing)
