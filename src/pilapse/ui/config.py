from dataclasses import dataclass

DEFAULT_FONT: str = "fonts/gohufont-14.pil"
DEFAULT_SPACING: int = 1
DEFAULT_BG_COLOR: int = 0x000000
DEFAULT_FG_COLOR: int = 0xffffff

@dataclass
class Config:
  width: int
  height: int
  font: str = DEFAULT_FONT
  spacing: int = DEFAULT_SPACING
  bg_color: int = DEFAULT_BG_COLOR
  fg_color: int = DEFAULT_FG_COLOR
