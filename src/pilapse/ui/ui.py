from enum import Enum, auto
from abc import ABC, abstractmethod
from dataclasses import dataclass
from control import KeyCode, KeyEvent
from state import GenericState, CharSelection, ListSelection, StringSelection

# class ControlState(Enum):
#   Continue = auto()
#   Finished = auto()

@dataclass
class GenericStateController(ABC):
  @abstractmethod
  def handle(self, state: GenericState, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int) -> GenericState:
    raise NotImplementedError

  # @result.setter
  # def result(self, result):
  #   self.__result = result

  # @property
  # def result(self):
  #   return self.__result

class CharSelectionController(GenericStateController):
  # def handle(self, char_selection: CharSelection, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int)
  def handle(self, state, key_code, key_event, ev_time_ns):
    if key_code == KeyCode.KEY_UP:
      state.select_next()
    elif key_code == KeyCode.KEY_DOWN:
      state.select_prev()
    return char_selection

class ListSelectionController(GenericStateController):
  # def handle(self, list_selection: list_selection, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int):
  def handle(self, state, key_code, key_event, ev_time_ns):
    if key_code == KeyCode.KEY_UP:
      state.select_next()
    elif key_code == KeyCode.KEY_DOWN:
      state.select_prev()
    elif key_code == KeyCode.KEY_PRESS:
      state.callback()
    return state

      # selection = list_selection.get_selection()[0]
      # if selection == 'quit':
      #   return ControlState.Finished
      # elif selection == 'input':
      #   return ControlState.Next
      # elif self.menu_state.current() == 'fonts':
      #   return ControlState.Next

        # list_selection.next_state = ???
      # elif self.menu_state.tag == 'Fonts':
      #   font = self.__fonts[self.menu_state.current()]
      #   self.__view.load_font(font)
      #   self.menu_state = self.states['Menu']

class StringSelectionController(GenericStateController):
  # def handle(self, string_selection: StringSelection, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int):
  def handle(self, state, key_code, key_event, ev_time_ns):
    if key_code == KeyCode.KEY_LEFT:
      state.select_prev()
    elif key_code == KeyCode.KEY_RIGHT:
      state.select_next()
    elif key_code == KeyCode.KEY1:
      state.delete()
    elif key_code == KeyCode.KEY2:
      state.backspace()
    elif key_code == KeyCode.KEY_PRESS:
      # self.result = string_selection.get_selection()
      return string_selection

class Controller:
  def __init__(self):
    self.__controller = \
      { StateType.CharSelection: CharSelectionController()
      , StateType.ListSelection: ListSelectionController()
      , StateType.StringSelection: StringSelectionController()
      }

  def register(self, state_type: StateType, controller: GenericStateController):
    self.__controller[state_type] = controller
