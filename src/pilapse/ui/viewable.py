from abc import ABC, abstractmethod
from typing import Generic, TypeVar, Type
from PIL import Image

V = TypeVar('V')

class Viewable(ABC, Generic[V]):
  @abstractmethod
  def type(self) -> Type[V]:
    raise NotImplementedError

  @abstractmethod
  def render(self) -> Image:
    raise NotImplementedError
