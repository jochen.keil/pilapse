# from abc import ABC, abstractmethod
from state import ListState, StringState
from string import printable
from typing import Sequence, Tuple, TypeVar
# from key_handler import KeyHandler
# from views import View, ListMenuView, StringInputView

# R = TypeVar('R')
#
# class Component:
#   def __init__(self, view: View, key_handler: KeyHandler):
#     self.__view = view
#     self.__key_handler = key_handler
#     self.__callbacks = []
#
#   def render(self, surface: Surface) -> None:
#     # self.__view.render(self, surface)
#     self.__view.render(surface)
#
#   def handle(self, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int) -> None:
#     r = self.__key_hander.handle(key_code, key_event, ev_time_ns)
#     for callback in self.__callbacks:
#       callback(self, r)
#
#   def register(self, callback: Callable[[Component, R], None]):
#     self.__callbacks.append(callback)
#
#   def deregister(self, callback: Callable[[Component, R], None]):
#     try:
#       self.__callbacks.remove(callback)
#     except ValueError:
#       pass

class ListMenu:
  def __init__( self, items: Sequence[Tuple[str, str]]):
    self.__state = ListState(items)

  def state(self) -> ListState:
    return self.__state

  def next(self) -> None:
    self.__state.next()

  def prev(self) -> None:
    self.__state.prev()

  def current_id(self) -> str:
    return self.__state.current_id()

class StringInput:
  def __init__(self, chars: str = printable):
    self.__state = StringState(chars=chars)

  def state(self) -> StringState:
    return self.__state

  def current_input(self) -> str:
    return self.__state.current()

  def next(self) -> None:
    self.__state.next()

  def prev(self) -> None:
    self.__state.prev()

  def next_char(self) -> None:
    self.__state.current_char_state().next()

  def prev_char(self) -> None:
    self.__state.current_char_state().prev()

  def reset(self) -> None:
    self.__state.reset()

  # def finish(self) -> None:
  #   self.callback(self.__state.current())
