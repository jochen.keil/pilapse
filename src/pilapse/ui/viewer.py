# from abc import ABC, abstractmethod
from typing import Dict, Type, TypeVar
# from PIL import Image
from views import View
from surface import Surface

T = TypeVar('T')
Views = Dict[Type[T], View]

class Viewer:
  def __init__(self, surface: Surface, views: Views = dict()) -> None:
    self.__views = views
    self.__surface = surface

  def register(self, obj: T, view: View) -> None:
    self.__views[id(obj)] = view

  def deregister(self, obj: T) -> None:
    try:
      self.__views.pop(id(obj))
    except KeyError:
      pass

  def run(self, obj: T) -> None:
    try:
      self.__views[id(obj)].render(obj, self.__surface)
    except KeyError:
      pass
