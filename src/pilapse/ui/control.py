import time
import RPi.GPIO as GPIO
from threading import Event, Thread
from enum import Enum, IntEnum, auto

class KeyCode(IntEnum):
  KEY1       = 21
  KEY2       = 20
  KEY3       = 16
  KEY_UP     = 6
  KEY_DOWN   = 19
  KEY_LEFT   = 5
  KEY_RIGHT  = 26
  KEY_PRESS  = 13

class KeyEvent(IntEnum):
  Hold    = auto()
  Press   = auto()
  Release = auto()

class KeyHold:

  class Runner(Thread):
    def __init__(self, key_code, handler, timeout, repeat, *args, **kwargs):
      super().__init__(daemon=True, *args, **kwargs)
      self.__cancel = Event()
      self.__key_code = key_code
      self.__handler = handler
      self.__timeout = timeout
      self.__repeat = repeat

    def cancel(self):
      self.__cancel.set()

    def run(self):
      time.sleep(self.__timeout / 1E3)
      count = 0
      while not self.__cancel.is_set() and (self.__repeat == 0 or count < self.__repeat):
        self.__handler(self.__key_code, KeyEvent.Hold, time.time_ns())
        time.sleep(self.__timeout / 1E3)
        count = count + 1

  def __init__(self, key_code, handler, timeout=2000, repeat=1):
    self.__key_code = key_code
    self.__handler = handler
    self.__timeout = timeout
    self.__repeat = repeat
    self.__ev_time_ns = 0
    self.__runner = None

  def handler(self, key_code, key_event, ev_time_ns):
    if key_code == self.__key_code:
      if key_event == KeyEvent.Press:
        self.__ev_time_ns = ev_time_ns
        self.__runner = KeyHold.Runner(key_code, self.__handler, self.__timeout, self.__repeat)
        self.__runner.start()
      if key_event == KeyEvent.Release:
        cancel = ev_time_ns - self.__ev_time_ns < self.__timeout * 1E6
        if self.__repeat == 0 or cancel:
          self.__runner.cancel()
          self.__runner = None

class KeyDispatcher:
  def __init__(self):
    self.__handler = dict()
    for kc in KeyCode:
      self.__handler[kc] = { KeyEvent.Press: [], KeyEvent.Release: [] }

  def register(self, key_code, key_event, handler):
    self.__handler[key_code][key_event].append(handler)

  def dispatcher(self, key_code, key_event, ev_time_ns):
    for kc in KeyCode:
      if key_code == kc:
        for handler in self.__handler[key_code][key_event]:
          handler(key_code, key_event, ev_time_ns)

class KeyLoop:
  # TODO
  # https://raspberrypi.stackexchange.com/questions/76667/debouncing-buttons-with-rpi-gpio-too-many-events-detected
  def __init__(self, bouncetime=100, fps=60):
    self.__fps = fps
    self.__stop = Event()
    self.__state = set()
    self.__handler = []

    GPIO.setmode(GPIO.BCM)
    for key_code in KeyCode:
      GPIO.setup(key_code, GPIO.IN, pull_up_down=GPIO.PUD_UP)
      # GPIO.add_event_detect(key_code, GPIO.BOTH, bouncetime=bouncetime)
      GPIO.add_event_detect(key_code, GPIO.BOTH)

  def __del__(self):
    GPIO.cleanup()

  def stop(self):
    self.__stop.set()

  def register(self, handler):
    self.__handler.append(handler)

  def deregister(self, handler):
    self.__handler.remove(handler)

  def run(self):
    while not self.__stop.is_set():
      t = time.time_ns()
      for key_code in KeyCode:
        if GPIO.event_detected(key_code):
          # key press
          if key_code not in self.__state:
            self.__state.add(key_code)
            for handler in self.__handler:
              handler(key_code, KeyEvent.Press, t)

          # key release
          elif key_code in self.__state:
            self.__state.discard(key_code)
            for handler in self.__handler:
              handler(key_code, KeyEvent.Release, t)

      time.sleep(1.0 / self.__fps)
