import time
import RPi.GPIO as GPIO
from typing import Dict, Type, TypeVar
from enum import Enum, IntEnum, auto
from keys import KeyCode, KeyEvent
from key_bindings import KeyBindings

T = TypeVar('T')
R = TypeVar('R')
Bindings = Dict[Type[T], KeyBindings[T]]

class KeyReader:
  # TODO
  # https://raspberrypi.stackexchange.com/questions/76667/debouncing-buttons-with-rpi-gpio-too-many-events-detected
  def __init__(self, key_bindings: Bindings = dict(), bouncetime=100):
    self.__state = set()
    self.__key_bindings = key_bindings

    GPIO.setmode(GPIO.BCM)
    for key_code in KeyCode:
      GPIO.setup(key_code, GPIO.IN, pull_up_down=GPIO.PUD_UP)
      # GPIO.add_event_detect(key_code, GPIO.BOTH, bouncetime=bouncetime)
      GPIO.add_event_detect(key_code, GPIO.BOTH)

  def __del__(self):
    GPIO.cleanup()

  def register(self, obj: T, key_bindings: KeyBindings[T]) -> None:
    self.__key_bindings[id(obj)] = key_bindings

  def deregister(self, obj: T) -> None:
    try:
      self.__key_bindings.pop(id(obj))
    except KeyError:
      pass

  def run(self, obj: T) -> R:
    t = time.time_ns()
    for key_code in KeyCode:
      if GPIO.event_detected(key_code):
        # key press
        if key_code not in self.__state:
          self.__state.add(key_code)
          try:
            bindings = self.__key_bindings[id(obj)]
            return bindings.handle(obj, key_code, KeyEvent.Press, t)
            # return self.__key_bindings.handle(obj, key_code, KeyEvent.Press, t)
          except KeyError:
            return None

        # key release
        elif key_code in self.__state:
          self.__state.discard(key_code)
          # return self.__key_bindings.handle(obj, key_code, KeyEvent.Release, t)
          try:
            bindings = self.__key_bindings[id(obj)]
            return bindings.handle(obj, key_code, KeyEvent.Release, t)
          except KeyError:
            return None
