from PIL import Image, ImageDraw, ImageFont

BG_COLOR=0xAAAA
FG_COLOR=0xFFFF

XSIZE = 1280
YSIZE = 640

def fill_with_outline(draw, width, height, outline, outline_width):
  box = [outline, outline, width - outline - 1, height - outline - 1]
  draw.rectangle(box, outline=FG_COLOR, fill=BG_COLOR, width=outline_width)

def font_test(draw, width, height):
  ttfs = ["Ubuntu-R.ttf", "Ubuntu-C.ttf", "Ubuntu-L.ttf", "Ubuntu-M.ttf", "Ubuntu-Th.ttf"]
  offset = 0
  for ttf in ttfs:
    font = ImageFont.truetype(ttf, 100)
    w, h = font.getsize(ttf)
    xpos = 0.5 * width
    ypos = offset * h + 0.1 * height
    draw.rectangle([xpos, ypos, xpos+w, ypos+h], outline=FG_COLOR, fill=BG_COLOR, width=1)
    # fill_with_outline(draw, width - width * 0.2, offset * h + 0.1 * h, 0, 1)

    draw.text((xpos, ypos), ttf, font=font, fill=FG_COLOR)
    offset = offset + 1

  # draw.text((10, 10), "Configuration", font=font, fill=0xFF)
  # font = ImageFont.truetype("Ubuntu-C.ttf", 100)
  # draw.text((0, 200), "Configuration", font=font, fill=0xFF)
  # font = ImageFont.truetype("Ubuntu-L.ttf", 100)
  # draw.text((0, 300), "Configuration", font=font, fill=0xFF)
  # font = ImageFont.truetype("Ubuntu-M.ttf", 100)
  # draw.text((0, 400), "Configuration", font=font, fill=0xFF)
  # font = ImageFont.truetype("Ubuntu-Th.ttf", 100)
  # draw.text((0, 500), "Configuration", font=font, fill=0xFF)

def main():
  img = Image.new(mode='I;16', size=(XSIZE, YSIZE), color=BG_COLOR)
  draw = ImageDraw.Draw(img)

  fill_with_outline(draw, XSIZE, YSIZE, 2, 1)
  font_test(draw, XSIZE, YSIZE)
  img.save('test.png')

if __name__ == "__main__":
  main()
