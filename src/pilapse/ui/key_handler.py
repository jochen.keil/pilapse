from abc import ABC, abstractmethod
from typing import TypeVar
from keys import KeyCode, KeyEvent

R = TypeVar('R')

class KeyHandler(ABC):
  @abstractmethod
  def handle(self, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int) -> R:
    raise NotImplementedError
