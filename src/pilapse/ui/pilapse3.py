from sys import exit
from time import sleep
from PIL import Image
# from core import Core
from viewer import Viewer
from surface import Surface
from context_manager import ContextManager
from keys import KeyCode, KeyEvent
from key_reader import KeyReader
from key_bindings import ListMenuKeyBindings, StringInputKeyBindings
from components import ListMenu, StringInput
from views import TextViewConfig, ListMenuView, StringInputView
from string import ascii_letters, digits, punctuation

DEFAULT_FONT: str = "fonts/gohufont-14.pil"
DEFAULT_SPACING: int = 1
DEFAULT_BG_COLOR: int = 0x000000
DEFAULT_FG_COLOR: int = 0xffffff
DEFAULT_XY_OFFSET: (int, int) = (0, 0)

MAIN_MENU_ITEMS = list(map(lambda s: (s.replace(' ', '_').lower(), s), \
    [ "Fonts", "Input", "Quit", "Sub Menu"
    , "Mode", "ISO", "Aperture", "Shutterspeed"
    , "Menu Item 1", "Menu Item 2", "Menu Item 3"
    , "Menu Item 4", "Menu Item 5", "Menu Item 6"
    , "Menu Item 7", "Menu Item 8", "Menu Item 9"
    , "Menu Item 10", "Menu Item 11", "Menu Item 12"
    , "Menu Item 13", "Menu Item 14", "Menu Item 15"
    ]))

SUB_MENU_ITEMS = list(map(lambda s: (s.replace(' ', '_').lower(), s), \
    map(lambda n: f'Item {n}', range(10))))

INPUT_CHARS = ascii_letters + digits + punctuation
INPUT_PROMPT = 'Please enter some text:'

# class PiLapseMenu:
#   def __init__(self, surface):
#     # menu components
#     self.main_menu = ListMenu(MAIN_MENU_ITEMS)
#     self.sub_menu = ListMenu(SUB_MENU_ITEMS)
#     self.string_input = StringInput(INPUT_CHARS)
#
#     # menu manager
#     self.menu_manager = MenuManager(self.main_menu)
#
#     self.viewer = Viewer()
#     self.viewer.register(self.main_menu, list_menu_view)
#     self.viewer.register(self.sub_menu, list_menu_view)
#     self.viewer.register(self.string_input, string_input_view)
#
#     # menu key bindings
#     self.key_bindings = KeyBindings()
#     self.key_handler = KeyHandler(self.key_bindings)
#
#     for key_event in [KeyEvent.Hold, KeyEvent.Release]:
#       self.key_bindings.bind(KeyCode.Up, key_event, self.main_menu.prev)
#       self.key_bindings.bind(KeyCode.Down, key_event, self.main_menu.next)
#     self.key_bindings.on_release(KeyCode.Press, self.main_menu_handler)
#
#     for key_event in [KeyEvent.Hold, KeyEvent.Release]:
#       self.key_bindings.bind(KeyCode.Up, key_event, self.sub_menu.prev)
#       self.key_bindings.bind(KeyCode.Down, key_event, self.sub_menu.next)
#     self.key_bindings.on_release(KeyCode.Press, self.sub_menu_handler)
#
#     for key_event in [KeyEvent.Hold, KeyEvent.Release]:
#       self.key_bindings.bind(KeyCode.Right, key_event, self.string_input.next)
#       self.key_bindings.bind(KeyCode.Left, key_event, self.string_input.prev)
#       self.key_bindings.bind(KeyCode.Up, key_event, self.string_input.prev_char)
#       self.key_bindings.bind(KeyCode.Down, key_event, self.string_input.next_char)
#     self.key_bindings.on_release(KeyCode.Press, self.string_input_handler)
#
#   def main_menu_handler(self) -> None:
#     current_id = self.list_menu.current_id()
#     print(main_menu_callback.__name__, current_id)
#     if current_id == 'input':
#       self.menu_manager.push(self.string_input)
#     elif current_id == 'quit':
#       exit()
#     elif current_id == 'menu_item_1':
#       self.menu_manager.push(self.sub_menu)
#
#   def sub_menu_handler(self) -> None:
#     current_id = self.list_menu.current_id()
#     print(sub_menu_handler.__name__, current_id)
#     if current_id == '1':
#       self.menu_manager.pop()
#
#   def string_input_handler(self) -> None:
#     current_input = self.string_input.current_input()
#     print(input_callback.__name__, current_input)
#     if current_input == 'q':
#       self.menu_manager.pop()
#     self.string_input.reset()
#
#   def render(self):
#     self.viewer.render(self.menu_manager.current())
#
#   def handle_keys(self):
#     self.key_handler.handle_keys(self.menu_manager.current())

def clear(surface) -> None:
  img = Image.new(size=(surface.width, surface.height), mode='1', color=0)
  surface.paste(img)

if __name__ == '__main__':

  with Surface.open('/dev/fb1', rotate=180) as surface:

    # text view config
    config = TextViewConfig( width = surface.width
                           , height = surface.height
                           , mode = '1'
                           , font = DEFAULT_FONT
                           , spacing = DEFAULT_SPACING
                           , bg_color = DEFAULT_BG_COLOR
                           , fg_color = DEFAULT_FG_COLOR
                           , xy_offset = DEFAULT_XY_OFFSET
                           )

    main_menu = ListMenu(MAIN_MENU_ITEMS)
    sub_menu = ListMenu(SUB_MENU_ITEMS)
    string_input = StringInput(INPUT_CHARS)

    context_manager = ContextManager(main_menu)

    list_menu_view = ListMenuView(config)
    string_input_view = StringInputView(config, prompt=INPUT_PROMPT)

    viewer = Viewer(surface)
    viewer.register(main_menu, list_menu_view)
    viewer.register(sub_menu, list_menu_view)
    viewer.register(string_input, string_input_view)

    list_menu_key_bindings = ListMenuKeyBindings()
    string_input_key_bindings = StringInputKeyBindings()

    key_reader = KeyReader()
    key_reader.register(main_menu, list_menu_key_bindings)
    key_reader.register(sub_menu, list_menu_key_bindings)
    key_reader.register(string_input, string_input_key_bindings)

    FPS = 60
    while True:
      current_component = context_manager.current()
      viewer.run(current_component)
      r = key_reader.run(context_manager.current())

      if r == 'quit':
        clear(surface)
        exit()
      elif r == 'sub_menu':
        context_manager.push(sub_menu)
      elif r == 'input':
        context_manager.push(string_input)
      elif r == 'item_2':
        context_manager.pop()
      elif current_component == string_input:
        if r is not None:
          print(r)
          current_component.reset()
          context_manager.pop()
      else:
        if r is not None:
          print(r)

    sleep(1.0 / FPS)

    # self.key_bindings = KeyBindings()
    # self.bind_main_menu(key_bindings)
    # self.bind_string_input(key_bindings)

    # pi_lapse_menu = PiLapseMenu(surface)

    # key_handler = KeyHandler(key_bindings)
    # key_handler.register(main_menu, main_menu.default_key_bindings())
    # key_handler.register(string_input, string_input.default_key_bindings())

    # while True:
    #   result = main_menu.run()
    #   if result == 'input':
    #     result = string_input.run()



    core = Core()
    core.register(viewer.run)
    core.register(key_handler.run)
    core.run()
