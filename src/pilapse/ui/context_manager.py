from typing import Optional, TypeVar
from stack import Stack
# from components import Component

T = TypeVar('T')

class ContextManager:
  def __init__(self, root):
    self.__root = root
    self.__items = Stack[T]()

  def current(self) -> T:
    if not self.__items.is_empty():
      return self.__items.top()
    else:
      return self.__root

  def pop(self) -> Optional[T]:
    if not self.__items.is_empty():
      return self.__items.pop()
    else:
      return None

  def push(self, component: T) -> None:
    self.__items.push(component)

#   def handle(self, key_code: KeyCode, key_event: KeyEvent, ev_time_ns: int) -> bool:
#     current = self.__root
#
#     if not self.__items.is_empty():
#       current = self.__items.top()
#
#     ret = current.handle(key_code, key_event, ev_time_ns)
#     if ret and not self.__items.is_empty():
#       self.__items.pop()
#
#     self.__view.render(current)
#
#     return ret

# def test():
#   from string import ascii_letters, digits, punctuation
#   from config import Config
#   from components import ListMenu, StringInput
#
#   def list_menu_callback(state: ListState) -> bool:
#     print('list_menu_callback', state.current())
#     return True
#
#   def string_input_callback(state: StringState) -> bool:
#     print('string_input_callback', state.current())
#     return True
#
#   lm = ListMenu(items=[('a', 'A'), ('b', 'B')], callback=list_menu_callback)
#   chars = ascii_letters + digits + punctuation
#   si = StringInput(chars=chars, callback=string_input_callback)
#
#   config = Config(width=0, height=0)
#   view = MenuView(config=config, canvas=None)
#
#   menu = Menu(root=lm, view=view)
#
#   print(menu.handle(KeyCode.Up, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Right, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Up, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Up, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Right, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Down, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Down, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Down, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Down, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Right, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Down, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Down, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Right, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Up, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Up, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Up, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Up, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Right, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Down, KeyEvent.Release, 0))
#   print(menu.handle(KeyCode.Down, KeyEvent.Release, 0))
#
#   print('menu.handle(KeyCode.Press, ..)', menu.handle(KeyCode.Press, KeyEvent.Release, 0))
