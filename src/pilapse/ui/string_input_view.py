from PIL import Image, ImageDraw
from typing import Optional
from components import StringInput
from text_view import TextView

class StringInputView(TextView):
  def __init__(self, surface, config, prompt: Optional[str] = None):
    super().__init__(surface, config)
    self.__prompt = prompt

  @property
  def prompt(self) -> Optional[str]:
    return self.__prompt

  def render(self, string_input: StringInput) -> None:
    self.clear()

    current_idx = self.string_input.current_char_index()
    cursor = current_idx * ' ' + '^'

    prompt = self.prompt + '\n' if self.prompt is not None else ''
    selection = self.string_input.get_selection()

    text = prompt + selection + '\n' + cursor
    _, text_height = self.text_size(text)

    x = self.xy_offset[0]
    y = (self.surface.height - text_height) / 2

    self.draw_text((x, y), text)
