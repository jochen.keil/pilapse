class Test1:
  def __init__(self):
    self.__t = None

  @property
  def test1(self):
    return self.__t

  @test1.setter
  def test1(self, t):
    self.__t = t

  def foo(self, bar):
    print(bar.test2())

class Test2:
  def __init__(self, test):
    self.test = test

  def test1(self):
    self.test.foo(self)

  def test2(self):
    return 'test2'

def test():
  t1 = Test1()
  t2 = Test2(t1)

  t2.test1()

class Test3:
  def __init__(self):
    self.test3 = 'hello'

class Test4(Test3):
  def foo(self):
    print(self.test3)

def kwargs_test(**kwargs):
  print(type(kwargs), kwargs)
