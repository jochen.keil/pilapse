# PiLapse

Raspberry Pi (Zero) which automatically adjust ISO, aperture and shutterspeed
for timelapse recordings

## Features

- e-Ink Display
- Button Shim
- External trigger input
- Camera connection via USB and WiFi

## Setup

```bash
$ git clone --recursive https://gitlab.com/jochen.keil/pilapse
$ cd pilapse
$ python3 -m venv env
$ source env/bin/activate
$ pip install build
$ cd button-shim/library
$ python3 -m build
$ pip install dist/buttonshim-0.0.2-py3-none-any.whl
$ cd ../../e-Paper/RaspberryPi_JetsonNano/python
$ python3 -m build
$ pip install dist/waveshare_epd-0.0.0-py3-none-any.whl
```
