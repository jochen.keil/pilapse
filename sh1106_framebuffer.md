# How to configure an SH1106 display on Raspberry Pi for Framebuffer

## Recent kernels include an SH1106 driver

> https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/drivers/staging/fbtft/fb_sh1106.c?h=v5.12

The overlays are documented here (search for sh1106-spi in the `README` file):

> https://github.com/raspberrypi/linux/tree/rpi-5.10.y/arch/arm/boot/dts/overlays


## Boot configuration:

### Enable SH1106 overlay

Append to `/boot/config.txt`:

```
dtoverlay=sh1106-spi
```

### Kernel command line options

> https://scribles.net/customizing-boot-up-screen-on-raspberry-pi/

Append to `/boot/cmdline.txt`

#### Redirect console to `/dev/fb1`

```
fbcon=map:10
```

#### Change console font

```
fbcon=font:ProFont6x11
```

#### Remove Raspberry Pi logo:

```
logo.nologo
```

#### Remove blinking cursor

This is important. If not disabled the cursor will periodically overwrite
anything on the framebuffer device.

```
vt.global_cursor_default=0
```

Alternatively on a console after booting (http://www.friendlyarm.net/forum/topic/2998):

```
echo -e '\033[?17;0;0c' > /dev/tty1
```

#### Disable boot message texts

```
quiet
```

#### Enable splash image (only for Plymouth / X11)

```
splash
```
